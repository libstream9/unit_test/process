#include <stream9/spawn_process.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/linux/error.hpp>

namespace testing {

namespace st9 = stream9;
namespace lx = st9::linux;

BOOST_AUTO_TEST_SUITE(spawn_)

    BOOST_AUTO_TEST_CASE(success_1_)
    {
        try {
            char const* args[] = {
                "echo", "hi",
            };

            st9::spawn_process(args);
        }
        catch (...) {
            st9::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        try {
            char const* args[] = {
                "./xxx"
            };

            st9::spawn_process(args);

            BOOST_TEST(false);
        }
        catch (st9::error const& e) {
            BOOST_TEST(e.why() == lx::errc::enoent);
        }
        catch (...) {
            st9::print_error();
            BOOST_TEST(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // spawn_

BOOST_AUTO_TEST_SUITE(spawn_detached_)

    BOOST_AUTO_TEST_CASE(success_1_)
    {
        try {
            char const* args[] = {
                "echo", "hi",
            };

            st9::spawn_detached_process(args);
        }
        catch (...) {
            st9::print_error();
            BOOST_TEST(false);
        }
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        try {
            char const* args[] = {
                "./xxx"
            };

            st9::spawn_detached_process(args);

            BOOST_TEST(false);
        }
        catch (st9::error const& e) {
            BOOST_TEST(e.why() == lx::errc::enoent);
        }
        catch (...) {
            st9::print_error();
            BOOST_TEST(false);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // spawn_detached_

} // namespace testing
